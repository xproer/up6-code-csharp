﻿using Newtonsoft.Json.Linq;
using System;

namespace csharpApp
{
    class FileSvr
    {
        public string id = Guid.NewGuid().ToString("N");//38bddf48f43c48588e0d78761eaa1ce6
        public string pid = string.Empty;
        public string pidRoot = string.Empty;
        public bool  f_fdTask = false;
        public bool f_fdChild = false;
        public string uid = string.Empty;
        public string nameLoc = string.Empty;
        public string nameSvr = string.Empty;
        public string pathLoc = string.Empty;
        public string pathSvr = string.Empty;
        public string pathRel = string.Empty;
        public string md5 = string.Empty;
        public long lenLoc = 0;
        public long lenSvr = 0;
        public string sizeLoc = "0byte";
        public string perSvr = "0%";
        public bool complete = false;
        public bool deleted = false;

        public void parse(JToken v)
        {
            this.id = v["id"].ToString();
            this.nameLoc = v["nameLoc"].ToString();
            this.pathLoc = v["pathLoc"].ToString();
            this.sizeLoc = v["sizeLoc"].ToString();
            this.lenLoc = Convert.ToInt64(v["lenLoc"].ToString());
        }
    }
}
