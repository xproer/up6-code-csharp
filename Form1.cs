﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace csharpApp
{
    public delegate void dgt_cbk(JToken o);
    public delegate void dgt_msg(string m);

    public partial class Form1 : Form
    {
        //全局队列表
        Dictionary<string, FileUploader> tasks = new Dictionary<string, FileUploader>();
        HttpUploaderAppLib.HttpPartitionClass up6 = null;
        JObject m_cfg = null;//全局配置

        public Form1()
        {
            InitializeComponent();

            this.up6 = new HttpUploaderAppLib.HttpPartitionClass();
            up6._IHttpPartitionEvents_Event_recvMessage += Up6__IHttpPartitionEvents_Event_recvMessage;
            var cfgPath = Directory.GetCurrentDirectory() + "\\config.js";
            this.m_cfg = JObject.Parse(File.ReadAllText(cfgPath));
            this.init_plug(cfgPath);
        }

        private void Up6__IHttpPartitionEvents_Event_recvMessage(string newVal)
        {
            this.tbMsg.AppendText(newVal);

            var o = JToken.Parse(newVal);
            var name = o["name"].ToString();
            if (name == "open_files")
            {
                this.ent_sel_files(o);
            }
            else if (name == "open_folders")
            {
                this.ent_sel_folder(o);
            }
            else if (name == "paste_files") this.ent_paste_files(o);
            else if (name == "post_process") this.ent_post_process(o);
            else if (name == "post_error") this.ent_post_error(o);
            else if (name == "post_complete") this.ent_post_complete(o);
            else if (name == "post_stoped") this.ent_post_stoped(o);
            else if (name == "scan_process") this.ent_scan_process(o);
            else if (name == "scan_complete") this.ent_scan_complete(o);
            else if (name == "md5_process") this.ent_md5_process(o);
            else if (name == "md5_complete") this.ent_md5_complete(o);
            else if (name == "md5_error") this.ent_md5_error(o);
            else if (name == "add_folder_error") { }
            else if (name == "load_complete")
            {
                this.ent_load_complete();
            }
            else if (name == "load_complete_edge")
            {
                this.ent_load_complete();
            }
        }
        
        private void btnFile_Click(object sender, EventArgs e)
        {
            this.sel_files();
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            this.sel_folder();
        }

        private void btnPost_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// 快速上传本地文件,
        /// 1.服务端快速生成路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPostLoc_Click(object sender, EventArgs e)
        {
            FileUploader up = new FileUploader(this.up6);
            up.show_msg = this.show_msg;
            up.m_cfg = this.m_cfg;
            FileSvr fs = new FileSvr();
            fs.pathLoc = "D:\\clipbrd.exe";
            this.tasks.Add(fs.id, up);//添加到队列
            up.post_file(fs);
        }

        public void show_msg(string m)
        {
            Action ac = () => {
            this.tbMsg.AppendText(m + "\n");
            };
            this.Invoke(ac);
        }

        void init_plug(string cfgFile)
        {
            var txt = File.ReadAllText(cfgFile);
            JObject o = new JObject();
            o["name"] = "init";
            o["config"] = JObject.Parse(txt);
            this.up6.postMessage(o.ToString());
        }
        void add_fileLoc(string path) {
            JObject o = new JObject();
            o["name"] = "add_file";
            o["pathLoc"] = path;//本地文件路径
            this.up6.postMessage(o.ToString());
        }
        void sel_files() {
            JObject o = new JObject();
            o["name"] = "open_files";
            this.up6.postMessage(o.ToString());
        }
        void sel_folder()
        {
            JObject o = new JObject();
            o["name"] = "open_folders";
            this.up6.postMessage(o.ToString());
        }
        void paste_file() { }

        void ent_load_complete() {
            this.tbMsg.AppendText("控件加载完毕\n");
        }
        void ent_sel_files(JToken o) {
            foreach (JToken f in o["files"].ToArray())
            {
                foreach (JProperty p in f)
                {
                    //nameLoc,sizeLoc,pathLoc,id,
                    this.tbMsg.AppendText( string.Format("{0}:{1}\n", p.Name, p.Value) );
                }
                FileUploader up = new FileUploader(this.up6);
                up.show_msg = this.show_msg;
                up.m_cfg = this.m_cfg;
                up.m_fileSvr.parse(f);
                this.tasks.Add(up.m_fileSvr.id, up);//添加到队列
                up.check_file();
            }
        }

        void ent_sel_folder(JToken o) { }
        void ent_paste_files(JToken o) { }
        void ent_post_process(JToken o)
        {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].post_process(o);
            }
        }
        void ent_post_error(JToken o)
        {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].post_error(o);
            }
        }
        void ent_post_complete(JToken o) {

            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].post_complete(o);
            }
        }
        void ent_post_stoped(JToken o) {

            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
            }
        }
        void ent_scan_process(JToken o)
        {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].scan_process(o);
            }
        }
        void ent_scan_complete(JToken o)
        {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].scan_complete(o);
            }
        }
        void ent_md5_process(JToken o)
        {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].md5_process(o);
            }
        }
        void ent_md5_complete(JToken o) {
            var id = o["id"].ToString();
            if(this.tasks.ContainsKey(id))
            {
                this.tasks[id].md5_complete(o);
            }
        }
        void ent_md5_error(JToken o) {
            var id = o["id"].ToString();
            if (this.tasks.ContainsKey(id))
            {
                this.tasks[id].md5_error(o);
            }
        }
    }
}
